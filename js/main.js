import { dataGlasses } from "../js/model/glassModel.js";

let renderListGlass = () => {
    let HTMLOutput = "";
    dataGlasses.forEach((glasses) => {
        HTMLOutput += ` <img src="${glasses.src}" class="vglasses__items col-4 img "onclick="getGlass('${glasses.virtualImg}','${glasses.brand}','${glasses.name}','${glasses.color}','${glasses.price}','${glasses.description}')">`;
    });

    document.querySelector("#vglassesList").innerHTML = HTMLOutput;
};

let getGlass = (virtualImg, brand, name, color, price, description) => {
    let html = "";
    document.getElementById("avatarGlass").style.backgroundImage =
        "url(" + virtualImg + ")";
    document.querySelector("#avatarGlass").style.opacity = 0.9;
    document.getElementsByClassName("vglasses__info")[0].style.display = "flex";
    html += `<div>
                <div>
                    <h4>${name} - ${brand} (${color}) </h4>
                </div>
                <div>  
                    <span class="price">$${price}</span>
                </div>   
                <p>${description}</p>
            </div>`;

    document.querySelector("#glassesInfo").innerHTML = html;
};
window.getGlass = getGlass;

let removeGlasses = (status) => {
    if (status == false) {
        document.querySelector("#avatarGlass").style.opacity = 0;
    } else {
        document.querySelector("#avatarGlass").style.opacity = 0.9;
    }
};
window.removeGlasses = removeGlasses;

window.onload = () => {
    renderListGlass();
};